const express = require('express');
const faker = require('faker');
const app = express();



// Recuperer la liste des utilisateurs
const users = [];

for(let i = 0; i < 10; i++) {
    users.push({
        firstname: faker.name.firstName(),
        lastname: faker.name.lastName(),
        email: faker.internet.email()
    });
}

console.log(users);

// Crée la version de l'APi
const versionApi = '/api';

// GET /api/users

app.get('/' , (req, res) => {
    res.json({
              data: users
    })
});

// GET /api/users:id

app.get('/:id' , (req, res) => {
    const id = req.params.id - 1;
    
    res.json({

        data: users[id] || null

    })
});


// POST /api/
// PUT /api/

app.listen(3000, () => console.log ('Listening on port 3000'));


